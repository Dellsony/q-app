import React from "react";

const ArticlePicker = ({tittle, image, content, alt}) => {
    return(
        <div>
            <h1>{tittle}</h1>
            <img src={image} alt={alt} />
            <p>{content}</p>
        </div>
    )
}

export default ArticlePicker;