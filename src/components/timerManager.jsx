import React, { useState } from 'react';
import Timer from './timer';

const TimerManager = () => {
    const [timers, setTimers] = useState([
        {
            id: 1,
            Tname: "Timer",
            Tbutton: "start timer"
        },
        {
            id: 2,
            Tname: "Meditation timer",
            Tbutton: "Start meditation"
        }
    ]);

    const [timerName, setTimerName] = useState();
    const [timerId, setTimerId] = useState(3);

    return (
        <div>
            <form onSubmit={(e) => {
                e.preventDefault();
                const copyTimers = [...timers];
                copyTimers.push(
                    {
                        id: timerId,
                        Tname: timerName,
                        Tbutton: timerName
                    }
                );
                setTimers(copyTimers);
                setTimerId(timerId + 1);
            }}>
                <input className="Timer"
                    type=""
                    value={timerName}
                    placeholder="Set the timer name"
                    onChange={(e) => setTimerName(e.target.value)}
                />
                <button>timer new</button>
            </form>

            {timers.map(timer => (
                <Timer
                    key={timer.id}
                    name={timer.Tname}
                    button={timer.Tbutton}
                />
            ))}

        </div >
    );
};

export default TimerManager;