import React, { useState, useEffect } from "react";

const quotes = [
    "dog",
    "cat",
    "parrot",
    "rat",
    "rabbit"
];

function Clicker() {
    const [i, setI] = useState(0);
    useEffect(() => {
        if (i === 5) {
            setI(0);
            console.log(i);
        }
    }
    );

    return (
        <div>
            <div className="App">
                <button
                    className="App"
                    onClick={() => setI(i + 1)}
                >
                    {quotes[i] + " - " + i}
                </button>
            </div>
        </div>

    );
}

export default Clicker;
