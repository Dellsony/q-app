import React, { useState, useEffect } from 'react';

const Timer = ({ name, button }) => {
    const [seconds, setSeconds] = useState();
    const [isTimerRunning, setIsTimerRunning] = useState(true);
    const [isAlert, setIsAlert] = useState(false);

    useEffect(() => {
        const secondsLeft = parseInt(seconds);
        if (isTimerRunning && secondsLeft > 0) {
            setTimeout(() => {
                setSeconds(secondsLeft - 1);
            }, 1000);
        }
        else {
            if (isAlert) {
                alert("Timer out");
                setIsAlert(false);
            }
            setIsTimerRunning(false);

        }
    }, [isTimerRunning, seconds]
    );

    return (
        <div className="Timer">
            <label> {name}:
                <input className="Timer"
                    disabled={isTimerRunning}
                    placeholder="set the timer value"
                    type="number"
                    value={seconds}
                    onChange={(event) => setSeconds(event.target.value)}
                />

            </label>
            <button
                onClick={() => {
                    setIsTimerRunning(true);
                    setIsAlert(true);
                }}
            >
                {button}
            </button>
        </div>
    );
};

export default Timer;