import React from 'react';
import './App.css';
import Articles from './dataBase/articles';
import Clicker from './components/clicker';
import TimerManager from './components/timerManager';



function App() {
	return (
		<div>
			<Clicker />
			<div className="App">
				<TimerManager />
				<Articles />
			</div>
		</div>
	);
}

export default App;