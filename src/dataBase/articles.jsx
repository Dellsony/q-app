import React from 'react';
import ArticlePicker from '../components/articlePicker';

const articles = [
	{
		id: 1,
		alt: "car girls - 1",
		title: "topic 1",
		content: "this is the topic one pic",
		image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-5KW_8lBUwDP4aQceCmWZEtMe_bZQLhzjx8NKTOvIVMGc_RlMnnDY8dyfwRqlVvIt5s8&usqp=CAU"
	},
	{
		alt: "car girls -2",
		id: 2,
		title: "topic 2",
		content: "this is the topic two pic",
		image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSm9lUe0B7g40-_Ir2Uh6Bt7Bstt3SvM1vvTqtNa49vAkMulftHKA0oAyW9UNf8feiNMOw&usqp=CAU"
	},
	{
		alt: "car girls - 3",
		id: 3,
		title: "topic 3",
		content: "this is the topic three pic",
		image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYV7aLEtqY7fX1BYDu0zdux7S6XnTYvowyUAiLpHJELmAE8jkdzjTNvczNdSC_2R4U-Cg&usqp=CAU"
	}
];

function Articles() {
	return (
		<div className="App">
			{articles.map(article => (
				<ArticlePicker
					key={article.id}
					tittle={article.title}
					content={article.content}
					image={article.image}
					alt={article.alt}
				/>
			))}
		</div>
	);
}

export default Articles;